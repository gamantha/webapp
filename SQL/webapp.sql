/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : webapp

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-06-21 17:44:15
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------

-- ----------------------------
-- Table structure for `organization`
-- ----------------------------
DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization` (
  `org_id` varchar(255) NOT NULL,
  `org_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of organization
-- ----------------------------

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `project_id` varchar(255) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `owner_org_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  KEY `fk_project_organization_1` (`owner_org_id`),
  CONSTRAINT `fk_project_organization_1` FOREIGN KEY (`owner_org_id`) REFERENCES `organization` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_image`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_image`;
CREATE TABLE `reviu_image` (
  `image_url` varchar(255) DEFAULT NULL,
  `image_description` varchar(255) DEFAULT NULL,
  `object_type` varchar(255) DEFAULT NULL,
  `object_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_image
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_location`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_location`;
CREATE TABLE `reviu_location` (
  `unit_id` varchar(255) DEFAULT NULL,
  `cluster` varchar(255) DEFAULT NULL,
  `blok` varchar(255) DEFAULT NULL,
  `jalan` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `alamat` text,
  `coord` text,
  KEY `fk_reviu_location_reviu_unit_1` (`unit_id`),
  CONSTRAINT `fk_reviu_location_reviu_unit_1` FOREIGN KEY (`unit_id`) REFERENCES `reviu_unit` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_location
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_ref_location`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_ref_location`;
CREATE TABLE `reviu_ref_location` (
  `project_id` varchar(255) DEFAULT NULL,
  `project_location_tree_json` text,
  KEY `fk_reviu_ref_location_project_1` (`project_id`),
  CONSTRAINT `fk_reviu_ref_location_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_ref_location
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_ref_tipe`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_ref_tipe`;
CREATE TABLE `reviu_ref_tipe` (
  `project_id` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL,
  `lt` int(11) DEFAULT NULL,
  `lb` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`,`tipe`),
  CONSTRAINT `fk_reviu_ref_tipe_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_ref_tipe
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_unit`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_unit`;
CREATE TABLE `reviu_unit` (
  `unit_id` varchar(255) NOT NULL,
  `project_id` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  `lt` int(11) DEFAULT NULL,
  `lb` int(11) DEFAULT NULL,
  PRIMARY KEY (`unit_id`),
  KEY `fk_reviu_unit_project_1` (`project_id`),
  CONSTRAINT `fk_reviu_unit_project_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_unit
-- ----------------------------

-- ----------------------------
-- Table structure for `reviu_unit_status`
-- ----------------------------
DROP TABLE IF EXISTS `reviu_unit_status`;
CREATE TABLE `reviu_unit_status` (
  `unit_id` varchar(255) NOT NULL,
  `status_key` varchar(255) DEFAULT NULL,
  `status_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`unit_id`),
  CONSTRAINT `fk_reviu_unit_status_reviu_unit_1` FOREIGN KEY (`unit_id`) REFERENCES `reviu_unit` (`unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reviu_unit_status
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for `user_access`
-- ----------------------------
DROP TABLE IF EXISTS `user_access`;
CREATE TABLE `user_access` (
  `user_id` int(11) DEFAULT NULL,
  `org_id` varchar(255) DEFAULT NULL,
  `access_level` varchar(255) DEFAULT NULL,
  KEY `fk_user_access_user_1` (`user_id`),
  KEY `fk_user_access_organization_1` (`org_id`),
  CONSTRAINT `fk_user_access_organization_1` FOREIGN KEY (`org_id`) REFERENCES `organization` (`org_id`),
  CONSTRAINT `fk_user_access_user_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_access
-- ----------------------------
